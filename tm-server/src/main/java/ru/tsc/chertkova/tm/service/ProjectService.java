package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.StatusNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnerService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    protected @NotNull IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Project result;
        try {
            IProjectRepository repository = getRepository(connection);
            result = repository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Project result;
        try {
            IProjectRepository repository = getRepository(connection);
            result = repository.create(userId, name, description);
            result.setDateBegin(dateBegin);
            result.setDateEnd(dateEnd);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String userId, @Nullable final String id,
                              @Nullable final String name, @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Project result;
        try {
            IProjectRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setName(name);
            result.setDescription(description);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Project result;
        try {
            IProjectRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findById(id) != null;
    }

}
