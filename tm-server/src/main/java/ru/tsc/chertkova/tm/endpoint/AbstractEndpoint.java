package ru.tsc.chertkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.model.Session;
import ru.tsc.chertkova.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @Nullable final Session session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final Role roleUser = session.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

}
