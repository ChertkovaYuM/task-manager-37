package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IUserOwnerService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnerService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected abstract IUserOwnerRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            result = repository.add(userId, model);
            if (result == null) throw new ModelNotFoundException();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, sort.getComparator());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            Optional<M> model = Optional.ofNullable(repository.findById(userId, id));
            return model.orElseThrow(ModelNotFoundException::new);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        final int result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            result = repository.getSize(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        @Nullable M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            result = repository.remove(userId, model);
            if (result == null) throw new ModelNotFoundException();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            result = repository.removeById(userId, id);
            if (result == null) throw new ModelNotFoundException();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

}
