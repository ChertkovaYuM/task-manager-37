package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}
