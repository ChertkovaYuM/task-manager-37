package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) throws AbstractException {
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> set(@NotNull List<M> models) {
        @NotNull final Connection connection = getConnection();
        @Nullable final List<M> result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> addAll(@NotNull List<M> models) {
        @NotNull final Connection connection = getConnection();
        @Nullable final List<M> result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.addAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(sort.getComparator());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.remove(model);
            if (result == null) throw new ModelNotFoundException();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            if (id == null || id.isEmpty()) throw new IdEmptyException();
            Optional<M> model = Optional.ofNullable(repository.findById(id));
            return model.orElseThrow(ModelNotFoundException::new);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.removeById(id);
            if (result == null) throw new ModelNotFoundException();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final List<M> models) {
        if (models == null || models.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final Connection connection = getConnection();
        final int result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.getSize();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

}
