package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, Date dateEnd);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
