package ru.tsc.chertkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    @Override
    public List<M> set(@NotNull final List<M> models) {
        clear();
        return addAll(models);
    }

    @NotNull
    @Override
    public List<M> addAll(@NotNull final List<M> models) {
        for (final M model : models) add(model);
        return models;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = "DELETE FROM public.\"" + getTableName() + "\"";
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\"";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> models = new ArrayList<>();
        @NotNull final String sqlCommand = "SELECT * FROM public.\"" + getTableName() +
                "\" ORDER BY " + comparator.toString().toUpperCase();
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sqlCommand);
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"ID\" = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M model = fetch(resultSet);
        statement.close();
        return model;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = "SELECT COUNT(1) FROM public.\"" + getTableName() + "\"";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        resultSet.next();
        final int size = resultSet.getInt(1);
        statement.close();
        return size;
    }

    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        @NotNull final String sql = "DELETE FROM public.\"" + getTableName() + "\" WHERE \"ID\" = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, model.getId());
        statement.executeUpdate();
        statement.close();
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @NotNull final Optional<M> model = Optional.ofNullable(findById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public void removeAll(@NotNull final List<M> list) {
        list.forEach(this::remove);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findById(id) != null;
    }

}
