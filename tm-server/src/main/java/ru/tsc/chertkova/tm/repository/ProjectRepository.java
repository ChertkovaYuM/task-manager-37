package ru.tsc.chertkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Objects;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_PROJECT";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public Project add(@Nullable Project project) {
        @NotNull final String sql = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?,?)",
                "public.\"" + getTableName() + "\"",
                "ID", "USER_ID", "NAME", "DESCRIPTION", "STATUS", "CREATED_DT", "STARTED_DT", "COMPLETED_DT"
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, project.getId());
            preparedStatement.setString(2, project.getUserId());
            preparedStatement.setString(3, project.getName());
            preparedStatement.setString(4, project.getDescription());
            preparedStatement.setString(5, project.getStatus().toString());
            preparedStatement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            preparedStatement.setTimestamp(7, new Timestamp(project.getDateBegin().getTime()));
            preparedStatement.setTimestamp(8, new Timestamp(project.getDateEnd().getTime()));
            preparedStatement.executeUpdate();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project update(@Nullable Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,WHERE ID = ?",
                "public.\"" + getTableName() + "\"",
                "ID", "USER_ID", "NAME", "DESCRIPTION", "STATUS", "CREATED_DT", "STARTED_DT", "COMPLETED_DT"
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, project.getUserId());
            preparedStatement.setString(2, project.getName());
            preparedStatement.setString(3, project.getDescription());
            preparedStatement.setString(4, project.getStatus().getDisplayName());
            preparedStatement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
            preparedStatement.setTimestamp(6, new Timestamp(project.getDateBegin().getTime()));
            preparedStatement.setTimestamp(7, new Timestamp(project.getDateEnd().getTime()));
            preparedStatement.setString(8, project.getId());
            preparedStatement.executeUpdate();
            return project;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("ID"));
        project.setName(row.getString("NAME"));
        project.setDescription(row.getString("DESCRIPTION"));
        project.setUserId(row.getString("USER_ID"));
        project.setStatus(Objects.requireNonNull(Status.toStatus(row.getString("STATUS"))));
        project.setCreated(row.getTimestamp("CREATED_DT"));
        project.setDateBegin(row.getTimestamp("STARTED_DT"));
        project.setDateEnd(row.getTimestamp("COMPLETED_DT"));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable String userId, @Nullable Project model) {
        @NotNull final Project project = new Project(model.getName(), model.getDescription());
        project.setUserId(userId);
        @NotNull final String sql = "INSERT INTO public.\"" + getTableName() +
                "\" (\"ID\", \"NAME\", \"DESCRIPTION\", \"STATUS\", \"CREATED_DT\", \"STARTED_DT\", \"COMPLETED_DT\", \"USER_ID\") VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
        statement.setTimestamp(6, new Timestamp(project.getDateBegin().getTime()));
        statement.setTimestamp(7, new Timestamp(project.getDateEnd().getTime()));
        statement.setString(8, project.getUserId());
        statement.executeUpdate();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
