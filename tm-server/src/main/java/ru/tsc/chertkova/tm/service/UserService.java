package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.RoleEmptyException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.repository.ProjectRepository;
import ru.tsc.chertkova.tm.repository.TaskRepository;
import ru.tsc.chertkova.tm.repository.UserRepository;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.sql.Connection;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    protected @NotNull IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    protected @NotNull ITaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    protected @NotNull IProjectRepository getProjectRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User remove(@Nullable final User model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            result = repository.findById(model.getId());
            if (result == null) throw new UserNotFoundException();
            @NotNull final String userId = result.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            repository.remove(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return false;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            result = repository.findByLogin(login);
            if (result == null) throw new UserNotFoundException();
            repository.remove(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(login);
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isEmailExist(email);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new EmailExistsException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            result = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (role == null) throw new RoleEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(role);
            result = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            result = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull User user = findById(userId);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            result = repository.update(user);
            if (result == null) throw new UserNotFoundException();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUser(@Nullable final String userId, @Nullable final String firstName,
                           @Nullable final String lastName, @Nullable final String middleName) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            result = repository.findById(userId);
            if (result == null) throw new UserNotFoundException();
            result.setFirstName(firstName);
            result.setMiddleName(middleName);
            result.setLastName(lastName);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User result = findByLogin(login);
        if (result == null) throw new UserNotFoundException();
        result.setLocked(true);
        update(result);
        return result;
    }

    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User result = findByLogin(login);
        if (result == null) throw new UserNotFoundException();
        result.setLocked(false);
        update(result);
        return result;
    }

}
