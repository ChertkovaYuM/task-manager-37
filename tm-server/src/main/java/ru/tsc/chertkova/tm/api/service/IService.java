package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
