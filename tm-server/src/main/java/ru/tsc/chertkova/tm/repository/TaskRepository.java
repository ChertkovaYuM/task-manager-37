package ru.tsc.chertkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_TASK";

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task add(@Nullable Task task) {
        @NotNull final String sqlCommand =
                String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?,?,?)",
                        "public.\"" + getTableName() + "\"",
                        "ID", "USER_ID", "NAME", "DESCRIPTION", "PROJECT_ID", "STATUS", "CREATED_DT", "STARTED_DT", "COMPLETED_DT"
                );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, task.getId());
            preparedStatement.setString(2, task.getUserId());
            preparedStatement.setString(3, task.getName());
            preparedStatement.setString(4, task.getDescription());
            preparedStatement.setString(5, task.getProjectId());
            preparedStatement.setString(6, task.getStatus().toString());
            preparedStatement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
            preparedStatement.setTimestamp(8, new Timestamp(task.getDateBegin().getTime()));
            preparedStatement.setTimestamp(9, new Timestamp(task.getDateEnd().getTime()));
            preparedStatement.executeUpdate();
        }
        return task;
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task update(@Nullable Task task) {
        @NotNull final String sqlCommand = String.format(
                "UPDATE %s SET %s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ? WHERE ID = ?"
                , "public.\"" + getTableName() + "\"",
                "ID", "USER_ID", "NAME", "DESCRIPTION", "PROJECT_ID", "STATUS", "CREATED_DT", "STARTED_DT", "COMPLETED_DT"
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, task.getUserId());
            preparedStatement.setString(2, task.getName());
            preparedStatement.setString(3, task.getDescription());
            preparedStatement.setString(4, task.getProjectId());
            preparedStatement.setString(5, task.getStatus().toString());
            preparedStatement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
            preparedStatement.setTimestamp(7, new Timestamp(task.getDateBegin().getTime()));
            preparedStatement.setTimestamp(8, new Timestamp(task.getDateEnd().getTime()));
            preparedStatement.setString(9, task.getId());
            preparedStatement.executeUpdate();
            return task;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setStatus(Status.toStatus(row.getString("STATUS")));
        task.setCreated(row.getTimestamp("CREATED_DT"));
        task.setDateBegin(row.getTimestamp("STARTED_DT"));
        task.setDateEnd(row.getTimestamp("COMPLETED_DT"));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable String userId, @Nullable Task model) {
        @NotNull final Task task = new Task(model.getName(), model.getDescription(), model.getProjectId());
        task.setUserId(userId);
        @NotNull final String sql = "INSERT INTO " + "public.\"" + getTableName() +
                "\" (\"ID\", \"NAME\", \"DESCRIPTION\", \"STATUS\", \"CREATED_DT\", \"STARTED_DT\", \"COMPLETED_DT\", \"USER_ID\", \"PROJECT_ID\") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(task.getCreated().getTime()));
        statement.setTimestamp(6, new Timestamp(task.getDateBegin().getTime()));
        statement.setTimestamp(7, new Timestamp(task.getDateEnd().getTime()));
        statement.setString(8, task.getUserId());
        statement.setString(9, task.getProjectId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

    @Override
    public @Nullable Task create(@Nullable final String userId,
                                 @Nullable final String name,
                                 @Nullable final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"USER_ID\" = ? AND \"PROJECT_ID\" = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

}
