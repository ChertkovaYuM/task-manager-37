package ru.tsc.chertkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M>
        implements IUserOwnerRepository<M> {

    public AbstractUserOwnerRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    protected abstract String getTableName();

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        findAll(userId).clear();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"ID\" = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) {
        @NotNull final List<M> models = new ArrayList<>();
        if (comparator == null) return findAll(userId);
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlCommand = "SELECT * FROM public.\"" +
                getTableName() + "\" WHERE \"USER_ID\" = ? ORDER BY " +
                comparator.toString().toUpperCase();
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"ID\" = ? AND \"USER_ID\" = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M model = fetch(resultSet);
        statement.close();
        return model;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        @NotNull final String sql = "SELECT COUNT(1) FROM public.\"" + getTableName() + "\" WHERE \"USER_ID\" = ?";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        resultSet.next();
        final int size = resultSet.getInt(1);
        statement.close();
        return size;
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return removeById(model.getId());
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

}
