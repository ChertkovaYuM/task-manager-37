package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M add(@Nullable M model);

    @NotNull
    List<M> addAll(@NotNull List<M> models);

    void clear();

    @NotNull
    List<M> set(@NotNull List<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M findById(@Nullable String id);

    int getSize();

    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    void removeAll(@Nullable List<M> models);

    @Nullable
    M update(@Nullable M model);

    boolean existsById(@Nullable String id);

}
