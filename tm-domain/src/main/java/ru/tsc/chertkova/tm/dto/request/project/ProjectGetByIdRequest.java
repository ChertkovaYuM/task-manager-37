package ru.tsc.chertkova.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectGetByIdRequest(
            @Nullable String token, @Nullable String id
    ) {
        super(token);
        this.id = id;
    }

}
