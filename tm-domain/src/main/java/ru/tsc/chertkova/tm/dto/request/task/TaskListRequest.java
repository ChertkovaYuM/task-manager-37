package ru.tsc.chertkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.dto.response.user.AbstractUserResponse;
import ru.tsc.chertkova.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor

public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(@Nullable String token, @Nullable Sort sort) {
        super(token);
        this.sort = sort;
    }

}
